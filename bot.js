'use strict';

const Discord = require('discord.js');
const client = new Discord.Client();
const yaml = require('yamljs');
const config = yaml.load('settings.yml');
const RequestService = require('./src/Services/RequestService');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  var args = msg.content.split(/ +/g);
  const cmd = args.shift().toLowerCase();
  const prefix = config.settings.prefix;

  if (msg.author.bot) return;

  switch(cmd) {
    case `${prefix}csgostat`:
      if (args.length > 0) {
        args = args.join(' ');

        msg.reply('Wait a seconds');
        getStatsCSGO(args).then(function (message) {
          msg.channel.send(message);
        });
      } else {
        msg.reply('You need to send the pseudo of the player');
      }
      break;
  }
});

client.login(config.settings.token);

async function getStatsCSGO(pseudo) {
  return await RequestService.searchRequest(pseudo);
}

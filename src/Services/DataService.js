'use strict';

const Discord = require('discord.js');

module.exports = {
  constructStatsCSGO: (data) => {
    const message = new Discord.MessageEmbed()
    .setAuthor(data.platformInfo.platformUserHandle, data.platformInfo.avatarUrl)
    .setTitle('CSGO STATS');

    for (const property in data.segments[0].stats) {
      message.addField(data.segments[0].stats[property].displayName, data.segments[0].stats[property].displayValue, true);
    }

    return message;
  }
}
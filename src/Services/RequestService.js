'use strict';

const axios = require('axios').default;
const DataService = require('./DataService');
const yaml = require('yamljs');
const config = yaml.load('settings.yml');

module.exports = {
  searchRequest: (pseudo) => {
    const searchHeaders = {
       method: 'GET',
       url: 'https://public-api.tracker.gg/v2/csgo/standard/search?platform=steam&query=' + pseudo,
         headers: {
           'TRN-Api-Key': config.settings.tracker_token
         }
    }

    return axios(searchHeaders)
      .then((response) => {
        if (response.data.data.length > 0) {
          const getDataHeaders = {
            method: 'GET',
            url: 'https://public-api.tracker.gg/v2/csgo/standard/profile/steam/' + response.data.data[0].platformUserIdentifier,
            headers: {
              'TRN-Api-Key': config.settings.tracker_token
            }
          }

          return module.exports.getDataRequest(getDataHeaders);
        } else {
          return 'Sorry but we cannot find an account associated with this nickname';
        }
      })
      .catch(() => {
        return 'The service is temporary disabled';
      });
  },

  getDataRequest: (headers) => {
    return axios(headers)
      .then((response) => {
        if (response.data.data) {
          return DataService.constructStatsCSGO(response.data.data);
        } else {
          return 'Sorry but we cannot find an account associated with this nickname';
        }
      })
      .catch(() => {
        return 'The service is temporary disabled';
      })
  }
}
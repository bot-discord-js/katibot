# KatiBot - Discord JS

**This is the KatiBot on Discord JS repository.**

## Documentation

This bot is used to give the stats of a CSGO player based on the <a href="https://discord.js.org/">Discord.JS</a> and <a href="https://tracker.gg/">tracker.gg API</a>

## Required

* Node 11+
* Tracker GG Account
* Discord Application in Dev portal

## How to run this

Create file settings.yml with settings.example.yml

* `nvm use`
* `npm install`
* `node bot.js`